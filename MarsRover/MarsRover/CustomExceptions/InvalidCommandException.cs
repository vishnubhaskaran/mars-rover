﻿namespace MarsRover
{
    using System;

    /// <summary>
    /// Exception that is invoked when the command is not valid
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class InvalidCommandException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidCommandException"/> class.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public InvalidCommandException(string msg)
            : base(msg)
        {
        }
    }
}
