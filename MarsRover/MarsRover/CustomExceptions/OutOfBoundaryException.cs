﻿namespace MarsRover
{
    using System;

    /// <summary>
    /// exception that is called when the rover moves out of the plateau
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class OutOfBoundaryException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OutOfBoundaryException"/> class.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public OutOfBoundaryException(string msg)
            : base(msg)
        {
        }
    }
}
