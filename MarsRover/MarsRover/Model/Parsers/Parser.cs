﻿namespace MarsRover.Model.Parsers
{
    
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Parses the input to Rover
    /// </summary>
    public class Parser : IParser
    {
        /// <summary>
        /// The rover commands.
        /// </summary>
        private Dictionary<string, Action> commands;

        /// <summary>
        /// Looks up the dictionary.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>Action based on the command</returns>
        public bool LookUp(string action)
        {
            return this.commands.ContainsKey(action);
        }

        /// <summary>
        /// Parses the input.
        /// </summary>
        /// <param name="commands">The commands</param>
        /// <returns>Returns list of action</returns>
        /// <exception cref="InvalidCommandException">Throws exception when there is an invalid command</exception>
        public List<Action> ParseInput(string commands,Dictionary<string, Action>roverCommands)
        {
            this.commands = roverCommands;
            List<Action> parsedCommands = new List<Action>();
            foreach (var command in commands)
            {
                if (this.LookUp(command.ToString()))
                {
                    parsedCommands.Add(this.commands[command.ToString()]);
                }
                else
                {
                    throw new InvalidCommandException(string.Format("The Command " + command.ToString() + " unable to comprehend"));
                }
            }

            return parsedCommands;
        }
    }
}
