﻿namespace MarsRover.Model.Parsers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IParser
    {
        List<Action> ParseInput(string commands,Dictionary<string,Action>roverCommands);
        bool LookUp(string action);
    }
}
