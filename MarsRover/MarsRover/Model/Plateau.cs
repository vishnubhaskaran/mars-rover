﻿namespace MarsRover.Model
{
    /// <summary>
    /// Plateau describes the area within which the rover can roam around
    /// </summary>
    public class Plateau
    {
        /// <summary>
        /// Gets or sets the top right x.
        /// </summary>
        /// <value>
        /// The top right x.
        /// </value>
        public int TopRightX { get; set; }
        
        /// <summary>
        /// Gets or sets the top right y.
        /// </summary>
        /// <value>
        /// The top right y.
        /// </value>
        public int TopRightY { get; set; }

        /// <summary>
        /// Gets or sets the bottom left y.
        /// </summary>
        /// <value>
        /// The bottom left y.
        /// </value>
        public int BottomLeftY { get; set; }
        
        /// <summary>
        /// Gets or sets the bottom left x.
        /// </summary>
        /// <value>
        /// The bottom left x.
        /// </value>
        public int BottomLeftX { get; set; }

        /// <summary>
        /// Determines whether plateau [contains] [the specified coordinate].
        /// </summary>
        /// <param name="coordinate">The coordinate.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified coordinate]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(Coordinates coordinate)
        {
            if (!(coordinate.Xcoordinate < this.BottomLeftX) && !(coordinate.Xcoordinate > this.TopRightX) && !(coordinate.Ycoordinate < this.BottomLeftY) && !(coordinate.Ycoordinate > this.TopRightY))
            {
                return true;
            }

            return false;
        }
    }
}
