﻿namespace MarsRover.Model
{
    using MarsRover.Model.Parsers;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Rover class
    /// </summary>
    public class Rover
    {
        /// <summary>
        /// The rover commands
        /// </summary>
        private Dictionary<string, Action> roverCommands;

        /// <summary>
        /// The parser
        /// </summary>
        private IParser parser;
        /// <summary>
        /// Initializes a new instance of the <see cref="Rover"/> class.
        /// </summary>
        /// <param name="plateau">The plateau.</param>
        /// <param name="coordinates">The coordinates.</param>
        /// <param name="direction">The direction.</param>
        public Rover(Plateau plateau, Coordinates coordinates, Direction direction,IParser parser)
        {
            this.CurrentDirection = direction;
            this.CurrentPosition = coordinates;
            this.CurrentPlateau = plateau;
            this.roverCommands = new Dictionary<string, Action>
            {
                { "M", this.Move },
                { "L", this.TurnLeft },
                { "R", this.TurnRight }
            };
            this.parser = parser;
        }

        /// <summary>
        /// Gets or sets the current position.
        /// </summary>
        /// <value>
        /// The current position.
        /// </value>
        public Coordinates CurrentPosition { get; set; }

        /// <summary>
        /// Gets or sets the current direction.
        /// </summary>
        /// <value>
        /// The current direction.
        /// </value>
        public Direction CurrentDirection { get; set; }

        /// <summary>
        /// Gets or sets the current plateau.
        /// </summary>
        /// <value>
        /// The current plateau.
        /// </value>
        public Plateau CurrentPlateau { get; set; }

        /// <summary>
        /// Rover Turns left.
        /// </summary>
        public void TurnLeft()
        {
            this.CurrentDirection = this.CurrentDirection.TurnLeft();
        }

        /// <summary>
        /// Rover Turns right.
        /// </summary>
        public void TurnRight()
        {
            this.CurrentDirection = this.CurrentDirection.TurnRight();
        }

        /// <summary>
        /// Moves Rover.
        /// </summary>
        public void Move()
        {
            Coordinates newCoordinate;
            newCoordinate = this.CurrentPosition.CreateNewPosition(this.CurrentDirection.GetUnitValueForX(), this.CurrentDirection.GetUnitValueForY());
            if (this.CurrentPlateau.Contains(newCoordinate))
            {
                this.CurrentPosition = newCoordinate;
            }
            else
            {
                var message = $"Rover is trying to move to [{newCoordinate.Xcoordinate} ,{newCoordinate.Ycoordinate}], which is out of the boundary";
                throw new OutOfBoundaryException(message);
            }
        }

        /// <summary>
        /// Runs the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        public void Navigate(string command)
        {
          
            List<Action> commandsRecieved;
            try
            {
                commandsRecieved = parser.ParseInput(command,roverCommands);
                foreach (var cmd in commandsRecieved)
                {
                    cmd.Invoke();
                }
            }
            catch (OutOfBoundaryException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidCommandException i)
            {
                Console.WriteLine(i.Message);
            }
        }

        public override string ToString()
        {
            string message = "Current Postion of the Rover is- " + "X:" + this.CurrentPosition.Xcoordinate + "Y:" + this.CurrentPosition.Ycoordinate + "Direction:" + this.CurrentDirection.ToString();
            return message;
        }
    }
}
