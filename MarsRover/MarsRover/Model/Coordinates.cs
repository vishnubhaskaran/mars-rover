﻿namespace MarsRover.Model
{
    /// <summary>
    /// Specifies the rover position
    /// </summary>
    public class Coordinates
    {
        /// <summary>
        /// Gets or sets the x coordinate.
        /// </summary>
        /// <value>
        /// The x coordinate.
        /// </value>
        public int Xcoordinate { get; set; }

        /// <summary>
        /// Gets or sets the y coordinate.
        /// </summary>
        /// <value>
        /// The y coordinate.
        /// </value>
        public int Ycoordinate { get; set; }

        /// <summary>
        /// Creates the new position.
        /// </summary>
        /// <param name="unitStepValueForX">The unit step value for x.</param>
        /// <param name="unitStepValueForY">The unit step value for y.</param>
        /// <returns>A new coordinate</returns>
        public Coordinates CreateNewPosition(int unitStepValueForX, int unitStepValueForY)
        {
            return new Coordinates()
            {
                Xcoordinate = this.Xcoordinate + unitStepValueForX,
                Ycoordinate = this.Ycoordinate + unitStepValueForY
            };
        }
    }
}
