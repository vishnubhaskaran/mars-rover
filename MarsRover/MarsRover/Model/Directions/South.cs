﻿namespace MarsRover.Model.Directions
{
    /// <summary>
    /// South Direction
    /// </summary>
    /// <seealso cref="MarsRover.Model.Direction" />
    public class South : Direction
    {
        /// <summary>
        /// Gets the unit value for x.
        /// </summary>
        /// <returns>unit value for x coordinate</returns>
        public override int GetUnitValueForX()
        {
            return 0;
        }

        /// <summary>
        /// Gets the unit value for y.
        /// </summary>
        /// <returns>unit value for the y coordinate</returns>
        public override int GetUnitValueForY()
        {
            return -1;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "South";
        }

        /// <summary>
        /// Turns left.
        /// </summary>
        /// <returns>Return East Direction</returns>
        public override Direction TurnLeft()
        {
            return new East();
        }

        /// <summary>
        /// Turns right.
        /// </summary>
        /// <returns>West direction</returns>
        public override Direction TurnRight()
        {
            return new West();
        }
    }
}
