﻿namespace MarsRover.Model.Directions
{
    /// <summary>
    /// East Direction
    /// </summary>
    /// <seealso cref="MarsRover.Model.Direction" />
    public class East : Direction
    {
        /// <summary>
        /// Gets the unit value for x.
        /// </summary>
        /// <returns>unit value for X coordinate</returns>
        public override int GetUnitValueForX()
        {
            return 1;
        }

        /// <summary>
        /// Gets the unit value for y.
        /// </summary>
        /// <returns>unit value for y coordinate</returns>
        public override int GetUnitValueForY()
        {
            return 0;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "East";
        }

        /// <summary>
        /// Turns left.
        /// </summary>
        /// <returns>Returns North</returns>
        public override Direction TurnLeft()
        {
            return new North();
        }

        /// <summary>
        /// Turns right.
        /// </summary>
        /// <returns>Returns South</returns>
        public override Direction TurnRight()
        {
            return new South();
        }
    }
}
