﻿namespace MarsRover.Model
{
    /// <summary>
    /// the cardinal direction
    /// </summary>
    public abstract class Direction
    {
        /// <summary>
        /// Gets the unit value for x.
        /// </summary>
        /// <returns> Returns the unit value for x coordinate</returns>
        public abstract int GetUnitValueForX();

        /// <summary>
        /// Gets the unit value for y.
        /// </summary>
        /// <returns>Returns the unit value for y coordinate</returns>
        public abstract int GetUnitValueForY();

        /// <summary>
        /// Turns to right.
        /// </summary>
        /// <returns>Returns the Direction towards right</returns>
        public abstract Direction TurnRight();

        /// <summary>
        /// Turns to left.
        /// </summary>
        /// <returns>Returns the Direction towards left</returns>
        public abstract Direction TurnLeft();
    }
}
