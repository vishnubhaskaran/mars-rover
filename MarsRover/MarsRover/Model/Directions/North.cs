﻿namespace MarsRover.Model.Directions
{
    /// <summary>
    /// North Direction
    /// </summary>
    /// <seealso cref="MarsRover.Model.Direction" />
    public class North : Direction
    {
        /// <summary>
        /// Gets the unit value for x.
        /// </summary>
        /// <returns> Zero for  X coordinate</returns>
        public override int GetUnitValueForX()
        {
            return 0;
        }

        /// <summary>
        /// Gets the unit value for y.
        /// </summary>
        /// <returns> One For y coordinate</returns>
        public override int GetUnitValueForY()
        {
            return 1;
        }

        public override string ToString()
        {
            return "North";
        }

        /// <summary>
        /// Turns left.
        /// </summary>
        /// <returns> West Direction</returns>
        public override Direction TurnLeft()
        {
            return new West();
        }

        /// <summary>
        /// Turns right.
        /// </summary>
        /// <returns>East Direction</returns>
        public override Direction TurnRight()
        {
            return new East();
        }
    }
}
