﻿namespace MarsRover.Model.Directions
{
    /// <summary>
    /// West Direction
    /// </summary>
    /// <seealso cref="MarsRover.Model.Direction" />
    public class West : Direction
    {
        /// <summary>
        /// Gets the unit value for x.
        /// </summary>
        /// <returns>Returns unit step value for x coordinate</returns>
        public override int GetUnitValueForX()
        {
            return -1;
        }

        /// <summary>
        /// Gets the unit value for y.
        /// </summary>
        /// <returns>returns unit step value for the y coordinate</returns>
        public override int GetUnitValueForY()
        {
            return 0;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "West";
        }

        /// <summary>
        /// Turns left.
        /// </summary>
        /// <returns>returns the south direction</returns>
        public override Direction TurnLeft()
        {
            return new South();
        }

        /// <summary>
        /// Turns right.
        /// </summary>
        /// <returns>returns the north direction</returns>
        public override Direction TurnRight()
        {
            return new North();
        }
    }
}
