﻿namespace MarsRover
{
    using System;
    using MarsRover.Model;
    using MarsRover.Model.Directions;
    using MarsRover.Model.Parsers;

    /// <summary>
    /// the entry point 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            Coordinates coordinates =
                new Coordinates
                {
                    Xcoordinate = 0,
                    Ycoordinate = 0
                };
            Plateau plateau = new Plateau()
            {
                TopRightX = 5,
                TopRightY = 5,
                BottomLeftX = 0,
                BottomLeftY = 0
            };
            Direction direction = new North();
            Rover rover = new Rover(plateau, coordinates, direction,new Parser());
            rover.Navigate("MMMM");
            Console.WriteLine(rover.ToString());
            Console.ReadKey();
        }
    }
}
