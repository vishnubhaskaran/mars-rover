﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover.Model.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarsRover.Test
{
    [TestClass]
    class ParserTest
    {
        Dictionary<string, Action> testDictionary = new Dictionary<string, Action>();
       [TestMethod]
        public void Parser_to_Check_if_the_parser_returns_the_action_corresponding_to_each_command()
        {
            testDictionary.Add("F", () => { Console.WriteLine("first"); });
            testDictionary.Add("S", () => { Console.WriteLine("Second"); });
            testDictionary.Add("T", () => { Console.WriteLine("third"); });
            IParser parser = new Parser();
            List<Action> actionsToDO;
            actionsToDO = parser.ParseInput("FST", testDictionary);
            Assert.AreEqual(parser.ParseInput("FST", testDictionary),actionsToDO);
        }
    }
}
